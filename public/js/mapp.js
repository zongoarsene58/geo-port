
// function initMap() {
//     const myLatlng = { lat: 5.835802824291771, lng: -5.366602731741509 };
//     const map = new google.maps.Map(document.getElementById("mapp"), {
//         zoom: 10,
//         center: myLatlng,
//     });


//     const marker = new google.maps.Marker({
//         position: myLatlng,
//         draggable: true,
//         animation: google.maps.Animation.DROP,
//         map,
//         title: "Projet 1",
//     });
//     map.addListener("center_changed", () => {
//         // 3 seconds after the center of the map has changed, pan back to the
//         // marker.
//         window.setTimeout(() => {
//         map.panTo(marker.getPosition());
//         }, 3000);
//     });
//     marker.addListener("click", () => {
//         toggleBounce;
//         map.setZoom(15);
//         map.setCenter(marker.getPosition());
       
//     });
 
//     }

//     function toggleBounce() {
//         if (marker.getAnimation() !== null) {
//           marker.setAnimation(null);
//         } else {
//           marker.setAnimation(google.maps.Animation.BOUNCE);
//         }
//       }



// This example displays a marker at the center of Australia.
// When the user clicks the marker, an info window opens.
// The maximum width of the info window is set to 200 pixels.
function initMap() {
  const uluru = { lat: 5.835802824291771, lng: -5.366602731741509 };
  const p1={ lat: 5.8375735261518225, lng: -5.356465363353114 };  
  const p2={ lat:5.841106, lng: -5.351528 };
  const p3={ lat: 5.840768,  lng: -5.349911 };
  const p4={ lat: 5.848922, lng: -5.341836 };  

  const map = new google.maps.Map(document.getElementById("mapp"), {
    zoom: 15,
    center: uluru,
  });

  // afficharge de texte pour chaque projet
  const contentString =
    '<div id="content">' +
    '<div id="siteNotice">' +
    "</div>" +
    '<h1 id="firstHeading" class="firstHeading">Récontruction de route</h1>' +
    '<div id="bodyContent">' +
    "<p><b></b>Progression <b></b>, " +
    "La plupart des routes sont conçues pour maximiser les matériaux accessibles sur le site." +
    "C’est ce qu’on appelle l’excavation courante ou l’excavation de roches compactes. Les déblais excavés sont déplacés par de grandes machines comme les bulldozers, excavatrices et camions. Toutefois, certains matériaux doivent parfois être importés sur le site. Ces matériaux sont désignés « matériaux d’emprunt ».  " +
    "La dernière étape du nivellement d’un chemin est la pose d’une couche de terre végétale sur les pentes et l’hydroensemencement. " +
    
    "</div>" +
    "</div>";


    const contentString1 =
    '<div id="content">' +
    '<div id="siteNotice">' +
    "</div>" +
    '<h1 id="firstHeading" class="firstHeading">Lotissement</h1>' +
    '<div id="bodyContent">' +
    "<p><b></b>Progression <b></b>, " +
    "La plupart des routes sont conçues pour maximiser les matériaux accessibles sur le site." +
    "C’est ce qu’on appelle l’excavation courante ou l’excavation de roches compactes. Les déblais excavés sont déplacés par de grandes machines comme les bulldozers, excavatrices et camions. Toutefois, certains matériaux doivent parfois être importés sur le site. Ces matériaux sont désignés « matériaux d’emprunt ».  " +
    "La dernière étape du nivellement d’un chemin est la pose d’une couche de terre végétale sur les pentes et l’hydroensemencement. " +
    
    "</div>" +
    "</div>";
    const contentString2 =
    '<div id="content">' +
    '<div id="siteNotice">' +
    "</div>" +
    '<h1 id="firstHeading" class="firstHeading">Contruction de chateau d eau</h1>' +
    '<div id="bodyContent">' +
    "<p><b></b>Progression <b></b>, " +
    "La plupart des routes sont conçues pour maximiser les matériaux accessibles sur le site." +
    "C’est ce qu’on appelle l’excavation courante ou l’excavation de roches compactes. Les déblais excavés sont déplacés par de grandes machines comme les bulldozers, excavatrices et camions. Toutefois, certains matériaux doivent parfois être importés sur le site. Ces matériaux sont désignés « matériaux d’emprunt ».  " +
    "La dernière étape du nivellement d’un chemin est la pose d’une couche de terre végétale sur les pentes et l’hydroensemencement. " +
    
    "</div>" +
    "</div>";


    const contentString3 =
    '<div id="content">' +
    '<div id="siteNotice">' +
    "</div>" +
    '<h1 id="firstHeading" class="firstHeading">Travaux d assainissement</h1>' +
    '<div id="bodyContent">' +
    "<p><b></b>Progression <b></b>, " +
    "La plupart des routes sont conçues pour maximiser les matériaux accessibles sur le site." +
    "C’est ce qu’on appelle l’excavation courante ou l’excavation de roches compactes. Les déblais excavés sont déplacés par de grandes machines comme les bulldozers, excavatrices et camions. Toutefois, certains matériaux doivent parfois être importés sur le site. Ces matériaux sont désignés « matériaux d’emprunt ».  " +
    "La dernière étape du nivellement d’un chemin est la pose d’une couche de terre végétale sur les pentes et l’hydroensemencement. " +
    
    "</div>" +
    "</div>";

    const contentString4 =
    '<div id="content">' +
    '<div id="siteNotice">' +
    "</div>" +
    '<h1 id="firstHeading" class="firstHeading">Construction du Dispensaire</h1>' +
    '<div id="bodyContent">' +
    "<p><b></b>Progression <b></b>, " +
    "La plupart des routes sont conçues pour maximiser les matériaux accessibles sur le site." +
    "C’est ce qu’on appelle l’excavation courante ou l’excavation de roches compactes. Les déblais excavés sont déplacés par de grandes machines comme les bulldozers, excavatrices et camions. Toutefois, certains matériaux doivent parfois être importés sur le site. Ces matériaux sont désignés « matériaux d’emprunt ».  " +
    "La dernière étape du nivellement d’un chemin est la pose d’une couche de terre végétale sur les pentes et l’hydroensemencement. " +
    
    "</div>" +
    "</div>";
  // Création de vu de description des projets
  const infowindow = new google.maps.InfoWindow({
    content: contentString,
    maxWidth: 300,
  });
  const infowindow1 = new google.maps.InfoWindow({
    content: contentString1,
    maxWidth: 300,
  });
  const infowindow2 = new google.maps.InfoWindow({
    content: contentString2,
    maxWidth: 300,
  });
  const infowindow3 = new google.maps.InfoWindow({
    content: contentString3,
    maxWidth: 300,
  });
  const infowindow4 = new google.maps.InfoWindow({
    content: contentString4,
    maxWidth: 300,
  });

  // Création des markers

  const marker = new google.maps.Marker({
    position: uluru,
    map,
    title: "Récontruction de route",
  });
  const marker1 = new google.maps.Marker({
    position: p1,
    map,
    title: "Lotissement",
  });
  const marker2 = new google.maps.Marker({
    position: p2,
    map,
    title: "Contruction de chateau d'eau",
  });
  const marker3 = new google.maps.Marker({
    position: p3,
    map,
    title: "Travaux d'assainissement",
  });  
  
  const marker4 = new google.maps.Marker({
    position: p4,
    map,
    title: "Construction du Dispensaire ",
  });

  marker.addListener("click", () => {
    infowindow.open({
      anchor: marker,
      map,
      shouldFocus: false,
    });
  });

  marker1.addListener("click", () => {
    infowindow.open({
      anchor: marker1,
      map,
      shouldFocus: false,
    });
  });

  marker2.addListener("click", () => {
    infowindow.open({
      anchor: marker2,
      map,
      shouldFocus: false,
    });
  });


marker3.addListener("click", () => {
  infowindow.open({
    anchor: marker3,
    map,
    shouldFocus: false,
  });
});

marker4.addListener("click", () => {
  infowindow.open({
    anchor: marker4,
    map,
    shouldFocus: false,
  });
});
}



